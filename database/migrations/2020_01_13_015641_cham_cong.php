<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChamCong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cham_cong',function(Blueprint $table) {
            $table->date('ngay');
            $table->interger('ma_giao_vien')->unsigned();
            $table->interger('ma_giao_vien')->references(ma)->on('giao_vien');
            $table->float('so_gio_day');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cham_cong');
    }
}
