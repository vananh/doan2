<?php
namespace App\Model;
use DB;
class Ke_toanModel
{
	public $ma_ke_toan;
	public $ten_ke_toan;
	public $gioi_tinh;
	public $ngay_sinh;
	public $email;
	public $mat_khau;
	public $dia_chi;
	static function get_all(){
		$array = DB::select("select * from ke_toan");
		return $array;
	}
	public function insert(){
		DB::insert("insert into ke_toan(ten_ke_toan,gioi_tinh,ngay_sinh,email,mat_khau,dia_chi) values (?,?,?,?,?,?)",[
			$this->ten_ke_toan,
			$this->gioi_tinh,
			$this->ngay_sinh,
			$this->email,
			$this->mat_khau,
			$this->dia_chi

		]);
	}
	public function get_login()
	{
		$array = DB::select('select * from ke_toan where email = ? and mat_khau = ?',[
			$this->email,
			$this->mat_khau
		]);
		return $array;
	}
	static function get_one($ma_ke_toan ){
		$array = DB::select('select * from ke_toan where ma_ke_toan = ?',[
			$ma_ke_toan
		]);
		return $array;
	}
	public function update()
	{
		DB::update("update ke_toan set
		 ten_ke_toan = ?,
		 gioi_tinh = ?,
		 ngay_sinh = ?,
		 email = ?,
		 mat_khau = ?,
		 dia_chi = ?
		 where ma_ke_toan = ?",[
			$this->ten_ke_toan,
			$this->gioi_tinh,
			$this->ngay_sinh,
			$this->email,
			$this->mat_khau,
			$this->dia_chi,
			$this->ma_ke_toan
		]);
	}
}
