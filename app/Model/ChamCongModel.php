<?php

namespace App\Model;

use DB;

class ChamCongModel
{
	public $ngay;
	public $ma_giao_vien;
	public $so_gio_day;
	
	static function get_all(){
		$array = DB::select('select * from cham_cong');
		return $array;
	}
	public function insert(){
		DB::insert("insert into cham_cong(ngay, ma_giao_vien, so_gio_day) values (?,?,?)",[
			$this->ngay,
			$this->ma_giao_vien,
			$this->so_gio_day
			

		]);
	}

			public function get_login(){
		$array = DB::select('select * from cham_cong where email = ? and mat_khau = ?',[
			$this->email,
			$this->mat_khau
		]);
		return $array;
	}
	static function get_one($ngay){
		$array = DB::select('select * from cham_cong where ngay = ?',[
			$ngay
		]);
		return $array[0];
	}
	public function update()
	{
		DB::update("update cham_cong set
		 ma_giao_vien = ?,
		 so_gio_day = ?,
		 where ngay = ?",[
			$this->ma_giao_vien,
			$this->so_gio_day,
			$this->ngay
		]);
	}
}