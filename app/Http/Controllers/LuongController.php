<?php
namespace App\Http\Controllers;

use App\Model\LuongModel;
use Illuminate\Http\Request;
class LuongController
{
	function view_all(){
		$array = LuongModel::get_all();
		return view('view_all',compact('array'));
	}
	function view_insert(){
		return view('view_insert');
	}
	function process_insert(Request $rq){
		$muc_luong = new LuongModel();
		$muc_luong->so_tien_tren_1_gio = $rq ->get('so_tien_tren_1_gio');
		$muc_luong->insert();
		return redirect()->route('muc_luong.view_all');
	} 
	function view_update($ma_muc_luong)
	{
		$each = LuongModel::get_one($ma_muc_luong);

		return view('view_update',compact('each'));
	}
	function process_update($ma_muc_luong, Request $rq)
	{
		$muc_luong = new LuongModel();
		$muc_luong->ma_muc_luong = $ma_muc_luong;
		$muc_luong->so_tien_tren_1_gio = $rq->get('so_tien_tren_1_gio');

		$muc_luong->update();

		return redirect()->route("muc_luong.view_all");
	}
}

