<?php

namespace App\Http\Controllers;

use App\Model\ChamCongModel;
use Illuminate\Http\Request;

class GiaoVienController
{
	function view_all(){
		$array = ChamCongModel::get_all();
		
		return view('cham_cong.view_all',compact('array'));
	}
	function view_insert(){
		return view('cham_cong.view_insert');
	}
	function process_insert(Request $rq){
		$cham_cong = new ChamCongModel();
		$cham_cong->ngay = $rq->get('ngay');
		$cham_cong->ma_giao_vien = $rq->get('ma_giao_vien');
		$cham_cong->so_gio_day = $rq->get('so_gio_day');
		
		
		$cham_cong->insert();

		return redirect()->route('cham_cong.view_all');
	}
	function view_update($ngay){
			$each=ChamCongModel::get_one($ngay);
				return view('view_update',compact('each'));
	}
	function process_update(Request $rq){
		$cham_cong = new ChamCongModel();
		$cham_cong->ngay = $rq->get('ngay');
		$cham_cong->ma_giao_vien = $rq->get('ma_giao_vien');
		$cham_cong->so_gio_day = $rq->get('so_gio_day');
		
		
		$cham_cong->insert();
		

		//return redirect()->route('giao_vien.view_all');
	}
}