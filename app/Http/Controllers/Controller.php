<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use App\Model\Ke_toanModel;
use App\Model\GiaoVienModel;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function test()
    {
        return view('layer.master');
    }
    public function login()
    {
        return view('login');
    }
    public function process_login(Request $rq)
    {
        $ke_toan           = new Ke_toanModel();
        $ke_toan->email    = $rq->get('email');
        $ke_toan->mat_khau = $rq->get('mat_khau');
        $array             = $ke_toan->get_login();


        if(count($array)==1){
            $rq->session()->put('ma_ke_toan',$array[0]->ma_ke_toan);
            $rq->session()->put('ten_ke_toan',$array[0]->ten_ke_toan);

            return redirect()->route('ke_toan.view_all_ke_toan');
        }
        else{
            $giao_vien         = new GiaoVienModel();
            $giao_vien->email    = $rq->get('email');
            $giao_vien->mat_khau = $rq->get('mat_khau');
            $array           = $giao_vien->get_login();

            if(count($array)==1){
                $rq->session()->put('ma_giao_vien',$array[0]->ma_giao_vien);
                $rq->session()->put('ten_giao_vien',$array[0]->ten_giao_vien);

                return redirect()->route('cham_cong.view_all');
            }
        }
        
        return redirect()->route('view_login')->with('error','Nhập sai rồi');
    }


        public function logout(Request $rq)
    {
        $rq->session()->flush();
        return redirect()->route('view_login')->with('success','Đăng xuất thành công');
    }
}
