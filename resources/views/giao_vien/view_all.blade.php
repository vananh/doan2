@extends('layer.master')
@section('body')
<body>
<a href="{{ route('giao_vien.view_insert') }}">
	Thêm
</a>
<table border="1" width="100%">
	<tr>
		<th>Mã giáo viên</th>
		<th>Tên giáo viên</th>
		<th>Giới tính</th>
		<th>Ngày sinh</th>
		<th>Email</th>
		<th>Mật khẩu</th>
		<th>Địa chỉ</th>
		<th>Sửa</th>
	</tr>
	@foreach ($array as $each)
		<tr>
			<td>{{$each->ma_giao_vien}}</td>
			<td>{{$each->ten_giao_vien}}</td>
			<td>
				@if ($each->gioi_tinh==1)
					Nam
				@else
					Nữ
				@endif
			</td>
			<td>{{$each->ngay_sinh}}</td>
			<td>{{$each->email}}</td>
			<td>{{$each->mat_khau}}</td>
			<td>{{$each->dia_chi}}</td>
			<td>
				<a href="{{ route('giao_vien.view_update',['ma_giao_vien' => $each->ma_giao_vien]) }}">
					<button>Sửa</button>
				</a>
			</td>

		</tr>
	@endforeach
</table>
@endsection